# MA600 项目技术文档

## 目录
- [MA600 项目技术文档](#ma600-项目技术文档)
  - [目录](#目录)
  - [简介](#简介)
  - [主要函数](#主要函数)
    - [Motor.h](#motorh)
    - [Motor.c](#motorc)
    - [main.c](#mainc)
    - [MA600.h](#ma600h)
    - [MA600.c](#ma600c)
  - [main 函数启动过程](#main-函数启动过程)
    - [详细描述](#详细描述)
      - [系统初始化](#系统初始化)
      - [配置系统时钟](#配置系统时钟)
      - [初始化外设](#初始化外设)
      - [用户代码初始化](#用户代码初始化)
      - [主循环](#主循环)

## 简介
本项目主要涉及 MA600 设备的初始化和控制，包括电机的初始化、PID 控制器的配置和使用等。本文档详细总结了项目中涉及的主要函数及其功能，并描述了 main 函数的启动过程。

## 主要函数

### Motor.h
该文件定义了电机相关的数据结构和函数。

- `Motor_HandleTypeDef`：电机的主要数据结构，包含电机的各种状态信息。
    - `lastPosition`：上次电机转子编码器值。
    - `sumPosition`：电机转子编码器值累计和。
    - `sumPositionLast`：上一个时刻电机转子编码器值累计和。
    - `position`：电机实时转子编码器值。
    - `positionTarget`：位置环的期望。
    - `speed`：电机实时速度。
    - `speedTarget`：速度环的期望。
    - `temperature`：电机温度。
    - `turns`：电机从上电开始转过的圈数。
    - `current`：电机电流。
    - `id`：电机id。
- `MotorPID_struct`：PID 控制器的数据结构，包含不同类型的 PID 控制器。
    - `Rudder_ENC_position`：舵电机编码器位置环。
    - `Rudder_ENC_speed`：舵电机编码器速度环。
    - `Rudder_MAG_position`：磁编码器位置环。
    - `Rudder_MAG_speed`：磁编码器速度环。
    - `Wheel_ENC_speed`：轮电机编码器速度环。
- `MotorData_struct`：包含舵电机和轮电机的状态信息。
    - `Rudder`：舵电机数据结构。
    - `Wheel`：轮电机数据结构。
- `Motor_HandleInit`：电机初始化函数。

### Motor.c
该文件实现了电机相关的初始化函数。

- `Motor_HandleInit`：初始化电机数据结构，将舵电机和轮电机的位置初始化为 -1。

```c
// filepath: /C:/Users/ASUS/Desktop/MA600/MA600_LocalFix/Libraries/device/Motor.c
#include "Motor.h"

// ...existing code...

void Motor_HandleInit(void)
{
    motor_data.Rudder.position  = -1;
    motor_data.Wheel.position   = -1;
}
```

### main.c
该文件包含主程序的入口和主要逻辑。

- `main`：主程序入口，负责系统初始化和主循环。
- `SystemClock_Config`：配置系统时钟。
- `Error_Handler`：错误处理函数。

```c
// filepath: /C:/Users/ASUS/Desktop/MA600/MA600_LocalFix/Core/Src/main.c
// ...existing code...

int main(void)
{
  // ...existing code...

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_CAN1_Init();
  MX_CAN2_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */

    CAN_FilterConfig();

    MA600_HandleInit(&TestMA600, &hspi1, SPI1_CS_GPIO_Port, SPI1_CS_Pin);
    
    ETYETX = MA600_Read_ETYETX(&TestMA600);
    
    BCT = MA600_Read_BCT(&TestMA600);
    
    FW = MA600_Read_FW(&TestMA600);
    
    ETYETX = MA600_Set_ETYETX(&TestMA600, 0, 1);
    
    BCT = MA600_Set_BCT(&TestMA600, 94);
    
    FW = MA600_Set_FW(&TestMA600, 8);
    
    MotorCurrent[0] = 140;
    
    fix_handle_init(&fix_handle_test, 64, fix_type_null);
    
    PID_InitTypeDef PID_ENC_INIT = {0};
    
    PID_ENC_INIT.mode = PID_MODE_POSITION;
    PID_ENC_INIT.outMax = 2000;
    PID_ENC_INIT.iOutMax = 1000;
    PID_ENC_INIT.kp = 2.5;
    PID_ENC_INIT.ki = 0.1;
    PID_ENC_INIT.kd = 1;
    
    PID_init(&motor_pid.Rudder_ENC_speed, &PID_ENC_INIT);

  // ...existing code...

  while (1)
  {
      HAL_Delay(1);
      
      PID_calc(&motor_pid.Rudder_ENC_speed, speed_target, motor_data.Rudder.speed);
      MotorCurrent[0] = motor_pid.Rudder_ENC_speed.out;
      
      CAN_TX_CAN2_0x200(MotorCurrent[0], MotorCurrent[1], MotorCurrent[2], MotorCurrent[3]);
      
      // ...existing code...
  }
  // ...existing code...
}
```

### MA600.h
该文件定义了 MA600 设备相关的数据结构和函数。

- `hMA600_TypeDef`：MA600 设备的主要数据结构，包含设备的各种状态信息。
    - `hspi`：SPI 句柄。
    - `spi_CS_GPIOx`：SPI 片选引脚的端口。
    - `spi_CS_Pin`：SPI 片选引脚的引脚。
    - `Angle`：记录的角度值。
    - `MultiTurn`：记录的多圈圈数。
    - `Speed`：记录的速度。
- `MA600_HandleInit`：MA600 设备初始化函数。
- `MA600_Get_Angle`：读取当前角度。
- `MA600_Read_Reg`：读取指定寄存器的值。
- `MA600_Write_Reg`：向指定寄存器写入数据。
- `MA600_Store_Single`：存储单个寄存器块值到NVM中。
- `MA600_Restore_All`：将NVM中的所有值恢复到寄存器中。
- `MA600_Clear_ErrFlags`：清除所有错误标志。
- `MA600_Read_PPT`：读取每周期脉冲数。
- `MA600_Set_PPT`：设置每周期脉冲数。
- `MA600_Read_IOMatrix`：读取 IO 功能选项。
- `MA600_Set_IOMatrix`：设置 IO 功能选项。
- `MA600_Set_MTSP`：设置返回值类型（多圈圈数或旋转速度）。
- `MA600_Get_Speed_rpm`：读取测量的转速（rpm）。
- `MA600_Get_MultiTurn`：读取测量的多圈圈数。
- `MA600_Read_BCT`：读取 BCT 修正值。
- `MA600_Set_BCT`：设置 BCT 修正值。
- `MA600_Read_ETYETX`：读取通道衰减使能情况。
- `MA600_Set_ETYETX`：设置通道衰减使能情况。
- `MA600_Read_FW`：读取滤波窗口值。
- `MA600_Set_FW`：设置滤波窗口值。

### MA600.c
该文件实现了 MA600 设备相关的函数。

- `MA600_HandleInit`：初始化 MA600 设备。
- `MA600_Get_Angle`：读取当前角度。
- `MA600_Read_Reg`：读取指定寄存器的值。
- `MA600_Write_Reg`：向指定寄存器写入数据。
- `MA600_Store_Single`：存储单个寄存器块值到NVM中。
- `MA600_Restore_All`：将NVM中的所有值恢复到寄存器中。
- `MA600_Clear_ErrFlags`：清除所有错误标志。
- `MA600_Read_PPT`：读取每周期脉冲数。
- `MA600_Set_PPT`：设置每周期脉冲数。
- `MA600_Read_IOMatrix`：读取 IO 功能选项。
- `MA600_Set_IOMatrix`：设置 IO 功能选项。
- `MA600_Set_MTSP`：设置返回值类型（多圈圈数或旋转速度）。
- `MA600_Get_Speed_rpm`：读取测量的转速（rpm）。
- `MA600_Get_MultiTurn`：读取测量的多圈圈数。
- `MA600_Read_BCT`：读取 BCT 修正值。
- `MA600_Set_BCT`：设置 BCT 修正值。
- `MA600_Read_ETYETX`：读取通道衰减使能情况。
- `MA600_Set_ETYETX`：设置通道衰减使能情况。
- `MA600_Read_FW`：读取滤波窗口值。
- `MA600_Set_FW`：设置滤波窗口值。

```c
// filepath: /C:/Users/ASUS/Desktop/MA600/MA600_LocalFix/Libraries/device/MA600.c
// ...existing code...

void MA600_HandleInit(hMA600_TypeDef *hMA600, SPI_HandleTypeDef *hspi, GPIO_TypeDef *spi_CS_GPIOx, uint32_t spi_CS_Pin)
{
    hMA600->hspi            = hspi;
    hMA600->spi_CS_GPIOx    = spi_CS_GPIOx;
    hMA600->spi_CS_Pin      = spi_CS_Pin;
    
    hMA600->Angle           = 0;
}

uint16_t MA600_Get_Angle(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Read_Reg(hMA600_TypeDef *hMA600, uint8_t RegAddr)
{
    // ...existing code...
}

uint8_t MA600_Write_Reg(hMA600_TypeDef *hMA600, uint8_t RegAddr, uint8_t Value)
{
    // ...existing code...
}

uint16_t MA600_Store_Single(hMA600_TypeDef *hMA600, uint8_t BlockIndex)
{
    // ...existing code...
}

uint16_t MA600_Restore_All(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint16_t MA600_Clear_ErrFlags(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint16_t MA600_Read_PPT(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint16_t MA600_Set_PPT(hMA600_TypeDef *hMA600, uint16_t NewPPTValue)
{
    // ...existing code...
}

uint8_t MA600_Read_IOMatrix(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Set_IOMatrix(hMA600_TypeDef *hMA600, uint8_t Type)
{
    // ...existing code...
}

uint8_t MA600_Set_MTSP(hMA600_TypeDef *hMA600, uint8_t Type)
{
    // ...existing code...
}

float MA600_Get_Speed_rpm(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

int16_t MA600_Get_MultiTurn(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Read_BCT(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Set_BCT(hMA600_TypeDef *hMA600, uint8_t BCTValue)
{
    // ...existing code...
}

uint8_t MA600_Read_ETYETX(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Set_ETYETX(hMA600_TypeDef *hMA600, uint8_t ETY, uint8_t ETX)
{
    // ...existing code...
}

uint8_t MA600_Read_FW(hMA600_TypeDef *hMA600)
{
    // ...existing code...
}

uint8_t MA600_Set_FW(hMA600_TypeDef *hMA600, uint8_t FWValue)
{
    // ...existing code...
}
```

## main 函数启动过程
1. **系统初始化**：调用 `HAL_Init` 函数初始化 HAL 库，复位所有外设，初始化 Flash 接口和 Systick。
2. **配置系统时钟**：调用 `SystemClock_Config` 函数配置系统时钟。
3. **初始化外设**：依次初始化 GPIO、DMA、CAN、SPI 和 UART 外设。
4. **用户代码初始化**：
    - 配置 CAN 过滤器。
    - 初始化 MA600 设备。
    - 读取并设置 MA600 的参数。
    - 初始化电机电流和 PID 控制器。
5. **主循环**：在主循环中，定时调用 `HAL_Delay` 函数延时，并计算 PID 控制器的输出，发送 CAN 消息。

### 详细描述

#### 系统初始化
- `HAL_Init`：初始化 HAL 库，复位所有外设，初始化 Flash 接口和 Systick。

#### 配置系统时钟
- `SystemClock_Config`：配置系统时钟，包括设置 HSE、PLL 和时钟分频器。

#### 初始化外设
- `MX_GPIO_Init`：初始化 GPIO 端口。
- `MX_DMA_Init`：初始化 DMA 控制器。
- `MX_CAN1_Init`：初始化 CAN1 控制器。
- `MX_CAN2_Init`：初始化 CAN2 控制器。
- `MX_SPI1_Init`：初始化 SPI1 接口。
- `MX_USART1_UART_Init`：初始化 USART1 接口。

#### 用户代码初始化
- `CAN_FilterConfig`：配置 CAN 过滤器。
- `MA600_HandleInit`：初始化 MA600 设备。
- `MA600_Read_ETYETX`：读取 MA600 的 ETYETX 参数。
- `MA600_Read_BCT`：读取 MA600 的 BCT 参数。
- `MA600_Read_FW`：读取 MA600 的 FW 参数。
- `MA600_Set_ETYETX`：设置 MA600 的 ETYETX 参数。
- `MA600_Set_BCT`：设置 MA600 的 BCT 参数。
- `MA600_Set_FW`：设置 MA600 的 FW 参数。
- `fix_handle_init`：初始化 fix_handle。
- `PID_init`：初始化 PID 控制器。

#### 主循环
- `HAL_Delay`：延时 1 毫秒。
- `PID_calc`：计算 PID 控制器的输出。
- `CAN_TX_CAN2_0x200`：发送 CAN 消息。



# MA600角度校准程序说明

 

硬件平台：

主控芯片（MCU）：STM32F105RCT6

磁编码器芯片：MA600

电机：3508电机 – C620电调

所使用磁钢：[Φ25-φ16-d5-N38-钕铁硼径向充磁磁环](https://item.taobao.com/item.htm?_u=t2dmg8j26111&id=560836549283&spm=a1z0k.7628869.0.0.7a8f37de3BbGSP) 参考价格 ￥20.00/颗

 

机械平台：

![img](C:\Users\ASUS\Desktop\MA600\MA600_LocalFix\assets\clip_image002.jpg)

磁编安装位置

AirGap_r：1.8mm

AirGap_z：1.5mm

 

仿真页面截图：

![img](C:\Users\ASUS\Desktop\MA600\MA600_LocalFix\assets\clip_image004.png)

 

 

 

 

历程重点使用到的文件：

“MA600.c” 路径：SDK\MA600_LocalFix\Libraries\device

“MA600.h” 路径：SDK\MA600_LocalFix\Libraries\device

“fix.c”   路径：SDK\MA600_LocalFix\Libraries\device

“fix.h”   路径：SDK\MA600_LocalFix\Libraries\device

“fun.c”  路径：SDK\MA600_LocalFix\Libraries\common

“fun.h”  路径：SDK\MA600_LocalFix\Libraries\common

 

 

说明：

程序在main函数中通过调用

MA600_HandleInit(&TestMA600, &hspi1, SPI1_CS_GPIO_Port, SPI1_CS_Pin);

对MA600结构体TestMA600执行初始化操作，

将SPI2以及 cs引脚记录到结构体中以供后续通信中使用

 

随后调用

  ETYETX = MA600_Set_ETYETX(&TestMA600, 0, 1);

BCT = MA600_Set_BCT(&TestMA600, 94);

向MA600的ETYETX寄存器写值开启X轴方向的增益调整

向MA600的BCT寄存器写值调整增益大小

上述两步操作的值通过仿真获得

![img](C:\Users\ASUS\Desktop\MA600\MA600_LocalFix\assets\clip_image006.jpg)

 

 

在这之后调用

fix_handle_init(&fix_handle_test, 64, fix_type_null);

来对修正句柄fix_handle_test进行初始化，此处的操作是设置为64点修正并且将修正方式设置为不修正；

 

具体的校准流程如下：

1. 先发送指令让电机匀速旋转，这里设置的期望转速为200转/分钟；
2. 当对程序中 check_flag 赋非零值时，程序会不断地判断当前磁编返回的值是否在0附近，当到达0附近后会记录此时的电机编码器值，并在之后的校准过程中以此为电机编码器值的零点（会通过fun.c 中 ENC2Deg() 函数将其换算到0 - 360°内）；
3. 在第二步执行完之后，会将get_fix_point 标志置 1；
4. 在can接收回调函数中，若检测到get_fix_point为1的话会开始记录修正点。开始的时候会根据记录点的个数设置采集的间隔，即几次回调采集一个采样点，这里可以有两种记录的方式，一种是直接存放在开辟的数组内，之后可以再向flash中存储；另一种是直接向flash中写入。这里使用第一种，开辟了fix_points_flash这一数组
5. 当采集完指定数量的修正点后，会调用

fix_load(&fix_handle_test, fix_points_flash, fix_check);

来将采集到的修正点装填到fix_handle_test中，装填的过程中除了将数据复制到结构体开辟的内存空间外，还会对修正点进行排序。函数中的第三个参数允许用户传入自定义的检查函数，以验证修正点的合理性，这里传入的是fix_check函数，可以剔除修正点集中的重复项。

6. 装填完成后手动将修正句柄的修正方式改为fix_type_interpolation（插值修正）

fix_handle_test.fix_type = fix_type_interpolation;

7. 至此校准完成，用户在使用时只需要调用

fix(&fix_handle_test, TestMA600.Angle);

就可以实现对原始采集值的修正，其中fix_handle_test为所使用的修正句柄，TestMA600.Angle为读回的原始

 

关于本地的修正

在提供的程序中使用了64个修正点，fix库允许用户根据自己的需要设置修正点的个数。

 

当前的程序暂时只提供了一种修正方式——分段插值

修正思路如下：

先找出原始值轴上与当前测量值最近的两个修正点，之后根据这两个点进行线性插值

k = (p2->real - p1->real) / (p1->raw - p1->raw)

​    b = (p1->real - p1->raw * k)

​    real = k * raw + b

 

下图为64点插值修正效果图：

![img](C:\Users\ASUS\Desktop\MA600\MA600_LocalFix\assets\clip_image008.jpg)

 

其中

蓝色为编码器原始读值

绿色为电机编码器读值

黄色为修正前误差

红色为修正后误差

#include "pid.h"

// 定义并初始化 PID 参数变量
Motor_PID_Params motor1_pid_params = MOTOR1_PID_PARAMS;
// 如果有其他电机，也可以继续初始化
// Motor_PID_Params motor2_pid_params = MOTOR2_PID_PARAMS;

// ...existing code...
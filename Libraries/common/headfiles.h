#ifndef __headfiles_h
#define __headfiles_h

#include "stdio.h"
#include "stdint.h"
#include "string.h"
#include "stdlib.h"
#include "math.h"

#include "stm32f1xx_hal.h"
#include "stm32f1xx_it.h"
#include "stm32f1xx_hal_conf.h"



#include "can.h"
#include "dma.h"
#include "spi.h"
//#include "tim.h"
#include "gpio.h"
#include "usart.h"
#include "main.h"


/* ============ common ============= */
#include "pid.h"
#include "Fun.h"

/* ============ device ============= */

#include "MA600.h"
#include "motor.h"
#include "fix.h"

/* ============ task =============== */

/* ============ application ======== */


#endif /* __headfiles_h */


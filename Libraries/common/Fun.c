#include "Fun.h"


/**
  * @name   ENC2Deg()
  * @brief  将编码器的值换算成角度
  * @call   External
  * @param  enc         编码器值
  * @param  enc_otc     编码器单圈计数值
  * @param  enc_zero    编码器的零点值
  * @RetVal deg         换算后的角度值
  */
float ENC2Deg(int enc, int enc_otc, int enc_zero)
{
    float deg = 0;
    
    enc = enc % enc_otc;    // 单圈
    
    // 根据零点对编码器值进行修正
    enc = enc - enc_zero;
    enc = enc < 0 ? (enc_otc + enc) : enc;
    
    // 计算出当前编码器值在一圈内换算为角度之后的值
    deg = 360.0 * enc / enc_otc;
    
    return deg;
}/* ENC2Deg() */










#include "Motor.h"

/*电机结构体*/
MotorPID_struct     motor_pid;
MotorData_struct    motor_data;

void Motor_HandleInit(void)
{
    motor_data.Rudder.position  = -1;
    motor_data.Wheel.position   = -1;
}
#ifndef __Motor_h
#define __Motor_h

#include "headfiles.h"
#include "pid.h"


typedef struct
{
  uint16_t          lastPosition;    /*上次电机转子编码器值*/

  long double 	    sumPosition;     /*电机转子编码器值累计和*/
    
  long double       sumPositionLast; /*上一个时刻电机转子编码器值累计和*/

  uint16_t 		    position;        /*电机实时转子编码器值*/

  long double		positionTarget;  /*位置环的期望*/

  int16_t		    speed;           /*电机实时速度*/

  float             speedTarget;     /*速度环的期望*/

  uint8_t           temperature;     /*电机温度*/

  int32_t 		    turns;           /*电机从上电开始转过的圈数*/

  int16_t           current;         /*电机电流*/
	
  uint16_t          id;              /*电机id*/

}Motor_HandleTypeDef;

typedef struct
{
    // 编码器闭环
	PID_HandleTypeDef Rudder_ENC_position;      // 舵电机编码器位置环
	PID_HandleTypeDef Rudder_ENC_speed;         // 舵电机编码器速度环
	PID_HandleTypeDef Rudder_MAG_position;      // 磁编码器位置环
	PID_HandleTypeDef Rudder_MAG_speed;         // 磁编码器速度环
    
    // 轮电机
    PID_HandleTypeDef Wheel_ENC_speed;          // 轮电机编码器速度环

}MotorPID_struct;

typedef struct
{
	Motor_HandleTypeDef Rudder;
	Motor_HandleTypeDef Wheel;
}MotorData_struct;

/*电机结构体*/
extern MotorPID_struct     motor_pid;
extern MotorData_struct    motor_data;


void Motor_HandleInit(void);

#endif /* __Motor_h */

